Rails.application.routes.draw do
  resources :images
  #get 'contactus/index'
  #resources :contactus
  get "aboutus" => 'welcome#aboutus'
  get "contactus" => 'welcome#contactus'

  post "contact" => "welcome#contactus_create"
  get 'aboutus/index'
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  get 'welcome/index'
  #devise_for :users
  root 'welcome#index'
  #get "users/password/changepassword" => 'users#changepassword'
  devise_for :users, controllers: { registrations: 'users/registrations'}
  #get "users/password/changepassword" => 'users/passwords#changepassword'

  devise_scope :user do
    get  'users/password/changepassword', :to => 'users/registrations#changepassword'
    put  'users/password/changepassword', :to => 'users/registrations#updatepassword'
    post 'users/create_user', :to => 'users/registrations#user_add'
  end

  post "images/upload" => "images#upload"
  post "images/image_add" => "images#image_add"
  get "images/image_info/:id" => "images#image_info"
  post "contact_add" => "welcome#contact_add"
  get "image_count" => "images#image_count"

  # authenticate :user, ->(user) { user.admin? } do
  #   mount Sidekiq::Web => '/sidekiq'
  # end

  #resources :gallery
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
#root : "home#index"
end
