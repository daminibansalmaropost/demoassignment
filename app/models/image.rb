require 'open-uri'
class Image < ApplicationRecord
  paginates_per 3
  validates_uniqueness_of :image_name
  has_one_attached :image

  # Bulk upload companies
  def self.import(file)
      CSV.foreach(file.path, headers: true) do |row|
          image_obj = Image.new
          image_obj.image_name = row[0]
          image_url = row[1]
          image_obj.image_url = image_url
          downloaded_image = open(image_url)
          image_obj.image.attach(io: downloaded_image  , filename: row[0])
          image_obj.save
      end
  end

  # before_save :grab_image
  #

end
