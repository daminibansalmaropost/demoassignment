# frozen_string_literal: true

class Users::RegistrationsController < Devise::RegistrationsController
  protect_from_forgery unless: -> { request.format.json? }
  # before_action :configure_sign_up_params, only: [:create]
  # before_action :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  # def new
  #   super
  # end

  # POST /resource
  def create
    #super
    #for small lettters
    pwd_part_1 = (0...2).map { (97 + rand(26)).chr }.join
    #for numbers
    pwd_part_2 = (0...7).map { rand(9)}.join
    #for capital letter
    pwd_part_3 = (65 + rand(26)).chr
    pwd = pwd_part_1 + pwd_part_2 + pwd_part_3

    params[:user].merge!('password' => pwd)
    params[:user].merge!('password_confirmation' => pwd)
    super

  end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  # def update
  #   super
  # end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_up_params
  #   devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute])
  # end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  # end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end

  def changepassword
    puts current_user
    if current_user.nil?
      redirect_to '/users/sign_in'
    else
      puts current_user.email
      @resource = User.where(:email => current_user.email).first
    end
  end

  def updatepassword
    puts params
    puts current_user.email
    if current_user.nil?
      redirect_to '/users/sign_in'
    else
      user=User.where(:email => current_user.email).first
      if user.valid_password?(params[:user][:old_password])
        if params[:user][:password].eql? params[:user][:password_confirmation]
          user.password=params[:user][:password]
          user.password_confirmation=params[:user][:password_confirmation]
          user.save!
          flash[:notice] = "Password changed successfully"
          redirect_to root_path
        else
          puts "not matched"
          flash[:alert] = "passwords not matched"
          redirect_to '/users/password/changepassword'
        end
      else
        flash[:alert] = "wrong old password"
        @resource = user
        redirect_to '/users/password/changepassword'
      end
    end
  end

  def user_add
    @user = User.new
    @user.email = params[:email]
    @user.password = Base64.decode64(params[:password]).force_encoding('UTF-8')
    # @user.password_confirmation = params[:password]
    # User.new({:email => "guy@gmail.com", :roles => ["admin"], :password => "111111", :password_confirmation => "111111" }).save(false)
    respond_to do |format|
      if @user.save
        format.json { render json: {"status":"SUCCESS","msg":"Saved successfully","status_code":200} }
      else
        format.json { render json: {"status":"ERROR","msg":@user.errors,"status_code":500} }
      end
    end
  end
end
