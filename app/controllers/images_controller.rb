require 'open-uri'
class ImagesController < ApplicationController
  protect_from_forgery unless: -> { request.format.json? }
    def index
      # @images = Image.all
      @images = Image.all.page params[:page]
    end

    def image_count
      sleep(2)
      @image_count = Image.all.count
      respond_to do |format|
        if @image_count>0
          format.json { render json: {"count":@image_count,"status_code":200} }
        else
          format.json { render json: {"count":0,"status_code":500} }
        end
      end
    end

    def new
      @image = Image.new
    end

    def show
      @id = params[:id]
      @image = Image.find(@id)
    end

    def create
      @image = Image.new(:image_name=>params[:image][:image_name])
      if @image.save
        @image.image.attach(params[:image][:image])
        redirect_to :action => :show, :id => @image.id
      else
        render 'new'
      end
    end

    def edit
      @image = Image.find(params[:id])
    end

    def update
      @image = Image.find(params[:id])
      if params[:image][:image_name]
        if @image.update_attribute(:image_name,params[:image][:image_name])
          #redirect_to action: "index"
          flash[:notice] = "Image name updated successfully"
          redirect_to action: "index"
        else
          render 'edit'
        end
      end
    end

    def upload
      Image.import(params[:image][:file])
      flash[:notice] = "Images uploaded successfully"
      redirect_to images_path #=> or where you want
    end

    def image_info
      @image = Image.find_by_id(params[:id])
      respond_to do |format|
        if @image
          format.json { render json: {"status":"SUCCESS","msg":"Data Retrieved Successfully","status_code":200,"data":{"Image Name":@image.image_name,"Image Url":@image.image_url}} }
        else
          format.json { render json: {"status":"ERROR","msg":"Error in retrieving the data","status_code":500,"data":{} }}
        end
      end
    end

    def image_add
      @image = Image.new
      @image.image_name = params[:image_name]
      @image.image_url = params[:image_link]
      downloaded_image = open(@image.image_url)
      @image.image.attach(io: downloaded_image  , filename: @image.image_name)
      respond_to do |format|
        if @image.save
          format.json { render json: {"status":"SUCCESS","msg":"Saved successfully","status_code":200} }
        else
          format.json { render json: {"status":"ERROR","msg":@image.errors,"status_code":500} }
        end
      end
    end

    private
    def image_params
      params.require(:image).permit(:image,:image_name)
  end
end
