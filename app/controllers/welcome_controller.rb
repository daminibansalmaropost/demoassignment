class WelcomeController < ApplicationController
  protect_from_forgery unless: -> { request.format.json? }

  def index
  end

  def aboutus
    set_tab :aboutus
  end

  def contactus
    set_tab :contactus
    @contact = Contact.new
  end

  def contactus_create
    set_tab :contactus
    @contact = Contact.new(contact_params)
    if @contact.save
      # Tell the UserMailer to send a welcome email after save
      #deliver_later async call#deliver_now
      ContactMailer.with(contact: @contact).welcome_email.deliver_later
      flash[:notice] = "We got your request, connect with you soon"
      redirect_to root_path
    else
      render 'contactus'
    end
  end

  def contact_add
    @contact = Contact.new
    @contact.name = params[:name]
    @contact.email = params[:email]
    @contact.mobile_no = params[:mobile_no]
    @contact.description = params[:description]
    respond_to do |format|
      if @contact.save
        format.json { render json: {"status":"SUCCESS","msg":"Saved successfully","status_code":200} }
      else
        format.json { render json: {"status":"ERROR","msg":@contact.errors,"status_code":500} }
      end
    end
  end

  private
  def contact_params
    params.require(:contact).permit(:name, :email, :mobile_no, :description)
  end
end
