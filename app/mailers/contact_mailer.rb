class ContactMailer < ApplicationMailer
  default from: 'daminibansalmaropost@gmail.com'

  def welcome_email
    @contact = params[:contact]
    @url  = 'http://localhost:3000/sign_up'
    mail(to: @contact.email, subject: 'Welcome to Maropost Site')
  end
end
